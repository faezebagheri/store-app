import 'dart:convert';
import 'dart:core';
import 'dart:core' as prefix0;

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../global/MainModel.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class NetworkManager {
  String api;
  Map<String, dynamic> req;
  Function onSuccess;
  Function onFailed;
  Function onStart;
  Function onEnd;
  BuildContext context;
  bool showDCDialog;
  Function onDisconnect;

  NetworkManager(
      {@required this.api,
      @required this.req,
      @required this.onSuccess,
      @required this.onFailed,
      @required this.onStart,
      @required this.onEnd,
      @required this.context,
      this.showDCDialog = true,
      this.onDisconnect});

  void _request() async {
    MainModel model = ScopedModel.of(context);
    onStart();
    print(api);
    await http
        .post(
      api,
      headers: {'Content-Type': 'application/json'},
      body: json.encode(req),
    ).then((response) {
      //ToDo check result coee
      var resultData = json.decode(response.body);
      print(resultData);
      if (resultData["ResultCode"] == null) {
        print(resultData.toString());
        dynamic response = {"ResultCode": "-4", "ResultText": "Api Error!\nBad Response."};
        showDialog(
            context: context,
            builder: (BuildContext ctx) {
              return AlertDialog(
                title: Text('Error'),
                content: Text(resultData.toString()),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.of(ctx).pop();
                    },
                  )
                ],
              );
            }
        );
        onFailed(response);
        onEnd();
      } else {
        if (resultData["ResultCode"] >= 1) { // success condition
          print("success");
          onSuccess(resultData);
          model.setToken(resultData['Token']);
        } else {
          showAlert("Username or password is wrong /  Kullanıcı Adı ve Şifre Hatalı");
          onFailed(resultData);
          onEnd();
        }
      }
    }).catchError((error) {
        dynamic response = {"ResultCode": "-1", "ResultText": "No Internet"};
        showAlert("Check your network connection / Ağ bağlantınızı kontrol edin");
        onFailed(response);
        onEnd();
    }).timeout(Duration(seconds: 20), onTimeout: () {
      print("Untruppted");
      dynamic response = {"ResultCode": "-3", "ResultText": "Time Out Happend"};
      showAlert("Connection Time Outed / Bağlantı Zaman Aşımı");
      onFailed(response);
      onEnd();
    });
    onEnd();
  }

  Function get request {
    return _request;
  }

  void showAlert(String message){
    Alert(
      context: context,
      type: AlertType.none,
      title: message,
      buttons: [
        DialogButton(
          child: Text(
            "OK",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          color: Theme.of(context).primaryColor,
          onPressed: () { Navigator.pop(context);},
          width: 120,
        )
      ],
    ).show();
  }
}
