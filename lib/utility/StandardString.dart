import 'package:intl/intl.dart';


formatDate(DateTime myDateTime) {
  var formatter = new DateFormat('EEEE, dd MMM');
  return formatter.format(myDateTime);

}

formatExitDate(DateTime myDateTime) {
  var formatter = new DateFormat('EE, dd MMM HH:mm');
  return formatter.format(myDateTime);

}

String standardTime(DateTime dateTime){
  String result = (dateTime.hour<10?"0":"")+dateTime.hour.toString()+":"+(dateTime.minute<10?"0":"")+dateTime.minute.toString();
  return result;
}

String standardDate(DateTime dateTime){
  String result = dateTime.year.toString()+"-"+(dateTime.month<10?"0":"")+dateTime.month.toString()+"-"+(dateTime.day<10?"0":"")+dateTime.day.toString();
  return result;
}

int getJulianDate(){

  int diffInDays = DateTime.now().difference(DateTime(DateTime.now().year,1,1)).inDays+1;
  return diffInDays;
}