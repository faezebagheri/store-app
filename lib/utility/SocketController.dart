import 'dart:convert';
import 'dart:io';


import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import '../global/MainModel.dart';
import '../global/MyController.dart';

class SocketController {
  String sendDataString;
  String tcpAddress;
  Function onStart;
  Function onFailed;
  Function onEnd;
  Function onSuccess;
  Function showBox;
  int timeOutSeconds;
  int port;
  BuildContext context;

  SocketController({
    @required this.context,
    @required this.sendDataString,
    @required this.tcpAddress,
    @required this.port,
    @required this.timeOutSeconds,
    @required this.onStart,
    @required this.onEnd,
    @required this.onFailed,
    @required this.onSuccess,
    this.showBox,
  });

  Socket socket;
  _connectSocket() async {

    onStart();
    socket = await Socket.connect(tcpAddress, port).catchError((e) {
      print("Error Connecting to socket is: " + e.toString());
      if (e.toString().indexOf("SocketException") > -1) {
        if (e.toString().contains('Connection timed out')) {} else {
          onFailed(e.toString());
          onEnd();
          showAlert(
              "Check your network connection / Ağ bağlantınızı kontrol edin");
        }
      }
    // ignore: missing_return
    }).timeout(Duration(seconds: timeOutSeconds), onTimeout: () {
      showAlert("Connection Time Outed / Bağlantı Zaman Aşımı");
      onEnd();
//       socket.destroy();
    });

    socket.listen((List<int> event) {
      print("Listened event is: " + utf8.decode(event).toString());
      if (utf8.decode(event).toString().split(';')[0] == "-1") {
        onFailed(utf8.decode(event).toString());
        showAlert(utf8.decode(event).toString().split(';')[1]);
      } else {
        onSuccess(utf8.decode(event).toString());
      }
      onEnd();
      showBox();
    }, onError: (e){
      showAlert("Check your network connection / Ağ bağlantınızı kontrol edin");
      onEnd();
      showBox();
      socket.destroy();
    });

    socket.add(utf8.encode(sendDataString));

    await Future.delayed(Duration(seconds: timeOutSeconds));
    socket.destroy();
    onEnd();
    showBox();
  }

  Function get connectSocket {
    return _connectSocket;
  }
  void showAlert(String message){
    Alert(
      context: context,
      type: AlertType.none,
      title: message,
      buttons: [
        DialogButton(
          child: Text(
            "OK",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          color: Theme.of(context).primaryColor,
          onPressed: () { Navigator.pop(context);},
          width: 120,
        )
      ],
    ).show();
  }
}
