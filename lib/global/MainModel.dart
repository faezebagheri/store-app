import '../screens/barcodeReaderScreen/BarcodeReaderModel.dart';
import '../screens/loginScreen/LoginModel.dart';
import '../screens/addBaggageScreen/AddBaggageModel.dart';
import '../screens/exitBaggage/ExitBaggageModel.dart';
import 'package:scoped_model/scoped_model.dart';

class MainModel extends Model
    with
        LoginModel , BarcodeReaderModel , AddBaggageModel , ExitBaggageModel {
  bool _loading = false;

  bool get loading {
    return _loading;
  }

  bool _showList=false;
  bool get showList=> _showList;
  void setShowList(bool showList){
    _showList = showList;
  }

  bool _showBox = false;
  bool get showBox => _showBox;
  void setShowBox(bool showBox){
    _showBox = showBox;
  }

  bool _successUpload= false;
  bool get successUpload => _successUpload;
  void setSuccessUpload(bool successUpload){
    _successUpload = successUpload;
  }

  String _token="";
  get token => _token;
  void setToken(String token) {
    _token = token;
    notifyListeners();
  }


  onStart() {
    _loading = true;
    _successUpload = false;
    notifyListeners();
  }

  onEnd() {
    _loading = false;
    notifyListeners();
  }

  onFailed(error) {
    print(error.toString());
  }

  setGlobalLoading(val) {
    _loading = val;
    notifyListeners();
  }

}
