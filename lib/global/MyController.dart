import 'dart:convert';
import 'package:intl/intl.dart';

import 'package:flutter/material.dart';
import 'MainModel.dart';
import '../utility/SocketController.dart';
import '../utility/Consts.dart';
import '../utility/StandardString.dart';

class MyController {
  MainModel model;
  String data ='';

  MyController({@required this.model,});

  onStartDefault() {
    print("start");
    model.setGlobalLoading(true);
  }

  onEndDefault() {
    print("end");
    model.setGlobalLoading(false);
  }

  onFailedDefault(error) {
    print(error.toString());
    model.setGlobalLoading(false);
  }

  onSuccessDefault(dynamic resultData) {
    print("Success");
  }

  Image getImage(){
    return Image.memory(base64Decode(model.image));
  }

  selectBaggage(BuildContext context , String title , String data) async{
    String reqData;
    if(title == "selectWithBarcode") {
      reqData= "fdcsDLU;" +
          data+
          ";endD";
    }
    else if(title == "selectWithBaggageID") {
      reqData= "fdcsDLU_BI;" +
          data+
          ";endD";
    }

    var socketController = SocketController(
      context: context,
      onStart: onStartDefault,
      onEnd: onEndDefault,
      onSuccess: (responseData) => onSuccessSelect(responseData , context ),
      onFailed: onFailedDefault,
      sendDataString: reqData,
      port: port,
      tcpAddress: tcpAddress,
      timeOutSeconds: 20,
    );

    socketController.connectSocket();
  }
  onSuccessSelect(String recievedData , BuildContext context ) {
    print('Connect Success');
    data = data + recievedData;
    if(recievedData.contains("endR")) {
      if (recievedData.contains("fdcsDMUL")) {
        model.setShowList(true);
        var listData = data.toString().split(";")[1];
        var splitData = listData.split("~");
        splitData.forEach((splitedData) {
          model.baggageIdList.add(splitedData.split("_")[0]);
          model.airlineList.add(splitedData.split("_")[1].toUpperCase());
          model.flightDateList.add(splitedData.split("_")[2]);
          model.flightNumberList.add(splitedData.split("_")[3]);
          if (splitedData.split("_")[4] == '') {
            model.exitDateTimeList.add('');
          } else {
            String timeString = splitedData.split("_")[4];
            DateTime time = DateFormat("EEE, dd MMM HH:mm").parse(
                timeString, true).toLocal();
            print(time.toIso8601String());
            model.exitDateTimeList.add(formatExitDate(time));
          }
        });
        model.setHasList(true);
        model.setImage('');
        Navigator.pushReplacementNamed(context, '/ExitBaggageListView');
      } else {
        var splitedData = data.toString().split(";");
        model.setHasList(false);
        model.setImage(splitedData[1].split('_')[0]);
        model.setFightNumber(splitedData[2]);
        model.setFlightDateString(splitedData[3]);
        model.setSequenceNumber(splitedData[4]);
        model.setAirline(splitedData[5]);
        model.setFullName(splitedData[6]);
        model.setDescription(splitedData[7]);
        model.setBaggageID(splitedData[8]);
        model.setRoute(splitedData[9]);
        if (splitedData[10] == '') {
          model.setExitDateTime(splitedData[10]);
        } else {
          String timeString = splitedData[10];
          DateTime time = DateFormat("EEE, dd MMM HH:mm").parse(
              timeString, true).toLocal();
          print(time.toIso8601String());
          model.setExitDateTime(formatExitDate(time));
        }
        if(model.showList) {
          Navigator.pushNamed(context, '/exitBaggageScreen');
        }else{
          Navigator.pushReplacementNamed(context, '/exitBaggageScreen');
        }
      }
      data='';
    }
  }
}
