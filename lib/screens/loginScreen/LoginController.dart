import 'package:baggagestore/utility/Consts.dart';
import 'package:baggagestore/utility/NetworkManager.dart';
import 'package:flutter/cupertino.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../global/MainModel.dart';
import '../../global/MyController.dart';

class LoginController extends MyController {
  MainModel model;

  LoginController(this.model);

  showErrorDialog(BuildContext context, String message) {
    Alert(
      context: context,
      type: AlertType.warning,
      title: "",
      desc: "Please Take a Photo.",
      buttons: [
        DialogButton(
          child: Text(
            "OK",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () { Navigator.pop(context); Navigator.pushNamed(context, '/takePictureScreen');},
          width: 120,
        )
      ],
    ).show();
  }


  login(BuildContext context, String username, String password) async{

    //build request object first
    final Map<String, dynamic> reqData = {
      "Domain": "App mobile",
      "Username": username.trim(),
      "Password": password.trim(),
      "AppName": "ABDCS",
      "VersionNum": "10",
      "Device": 'android',
      "VersionJSNum": 20
    };

    var networkManager = NetworkManager(
        api: rootAPI + signInAPI,
        onEnd: onEndDefault,
        onFailed: onFailedDefault,
        onStart: onStartDefault,
        onSuccess: (responseData) => onSuccessLogin(context, responseData, username, password),
        req: reqData,
        context: context);

    try {
      await networkManager.request();
      final prefs = await SharedPreferences.getInstance();
      prefs.setString('Token', model.token);
      prefs.setString('Username', model.username);
      prefs.setString('Password', model.password);
    }catch(e){throw e;}
  }

  onSuccessLogin(BuildContext context, dynamic resultData, String username, String password) {
    print("Login Success");
    Navigator.of(context).pushReplacementNamed('/selectScreen');
  }
}
