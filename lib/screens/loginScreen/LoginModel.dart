import 'package:scoped_model/scoped_model.dart';
import 'package:flutter/material.dart';


mixin LoginModel on Model {

  String _username;
  String get username => _username;
  void setUsername (String username){
    _username = username;
    notifyListeners();
  }

  String _password;
  String get password => _password;
  void setPassword (String password){
    _password = password;
    notifyListeners();
  }

}