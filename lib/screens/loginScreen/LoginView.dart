import '../../global/MainModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'LoginController.dart';
import '../../utility/Consts.dart';


class LoginView extends StatefulWidget {
  final LoginController myLoginController;
  LoginView(this.myLoginController);

  @override
  State<StatefulWidget> createState() {
    return _LoginViewState();
  }
}

class _LoginViewState extends State<LoginView> {

  final GlobalKey<FormState> _formKey = GlobalKey();
  bool _obscureText = true;
  @override
  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: Scaffold(
        body: ScopedModelDescendant<MainModel>(builder: (context , child , model){
            return Container(
              padding: EdgeInsets.all(16.0),
              child: Center(
                child: _buildForm(model),
              ),
            );}
        ),
      ),
    );
  }

  Widget _buildForm(MainModel model){
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _buildUsernameTextField('Username / Kullanıcı adı', model),
            new SizedBox(height: 20.0),
            _buildPasswordTextField('Password / Şifre', model),
            new SizedBox(height: 30.0),
            model.loading
                ? Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CircularProgressIndicator(backgroundColor: Theme.of(context).primaryColor,),
                )
                : Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: _buildLoginButton(context, model),
                ),
          ],
        ),
      ),
    );
  }

  Widget _buildUsernameTextField(String inputValue , MainModel model){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 20,bottom: 3),
          child: Text(inputValue ,style: TextStyle(fontSize: 15,color: Theme.of(context).primaryColor),),
        ),
        TextFormField(
          style: TextStyle(
            fontSize: 19,
            color: Colors.black,
          ),
          decoration: InputDecoration(
            filled: true,
            fillColor: textBoxColor,
            contentPadding:
            EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(32.0),
            ),
          ),
          onChanged: (value) {
              model.setUsername(value);
          },
          onSaved: (value) {
            model.setUsername(value);
          },
        ),
      ],
    );
  }

  Widget _buildPasswordTextField(String inputValue, MainModel model){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 20,bottom: 3),
          child: Text(inputValue ,style: TextStyle(fontSize: 15,color: Theme.of(context).primaryColor),),
        ),
        TextFormField(
          style: TextStyle(
            fontSize: 19,
            color: Colors.black,
          ),
          obscureText: _obscureText,
          decoration: InputDecoration(
            filled: true,
            fillColor: textBoxColor,
            contentPadding:
            EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(32.0),
            ),
            suffixIcon:GestureDetector(
              onTap: () {
                setState(() {
                  _obscureText = !_obscureText;
                });
              },
              child: Icon(
                _obscureText
                    ? Icons.visibility
                    : Icons.visibility_off,
                semanticLabel: _obscureText
                    ? 'show password'
                    : 'hide password',
                color: Theme.of(context).primaryColor,
              ),
            ),
          ),
          onChanged: (value) {
              model.setPassword(value);
          },
          onSaved: (value) {
            model.setPassword(value);
          },
        ),
      ],
    );
  }

  Widget _buildLoginButton(BuildContext context , MainModel model){
    return RaisedButton(
      color: Theme.of(context).primaryColor,
      child: Text(
        'Log In',
        style: TextStyle(
          fontSize: 17,
          fontWeight: FontWeight.bold,
          color: Colors.white
        ),
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(24),
      ),
      onPressed: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        _formKey.currentState.save();
      widget.myLoginController.login(context,model.username,model.password);
      },
      padding:
      EdgeInsets.symmetric(horizontal: 30.0, vertical: 4),
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
    );
  }
}