import 'package:flutter/material.dart';
import 'SplashController.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashView extends StatefulWidget {
  final SplashController splashController;
  SplashView(this.splashController);
  @override
  _SplashViewState createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> with TickerProviderStateMixin {

  Animation<double> fadeInAnimation;
  AnimationController fadeInAnimationController;
  @override
  void initState() {
    fadeInAnimationController = AnimationController(
        duration: const Duration(milliseconds: 800), vsync: this);
    fadeInAnimation = Tween<double>(begin: 1.0, end: 0.0).animate(
        CurvedAnimation(
            parent: fadeInAnimationController, curve: Curves.easeIn))
      ..addListener(() {
        setState(() {
          // The state that has changed here is the animation object’s value.
        });
      });
    fadeInAnimationController.forward(from: 0);

    initOfSplashScreen();

    super.initState();
  }

  initOfSplashScreen() async {
    await Future.delayed(Duration(seconds: 4));
    getStringValuesSF();
  }

  getStringValuesSF() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String token = prefs.getString('Token');
    String username = prefs.getString('Username');
    String password = prefs.getString('Password');
    if (token != "" && token != null) {
      widget.splashController.model.setToken(token);
      widget.splashController.model.setUsername(username);
      widget.splashController.model.setPassword(password);
      print('token: ' + widget.splashController.model.token);
      print('username: ' + widget.splashController.model.username);
      print('password: ' + widget.splashController.model.password);
      Navigator.of(context).pushNamedAndRemoveUntil(
          '/selectScreen', (Route<dynamic> route) => false);
    } else {
      Navigator.of(context).pushNamedAndRemoveUntil(
          '/loginScreen', (Route<dynamic> route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            height: double.infinity,
            width: double.infinity,
            child: Image.asset("assets/images/HavatechLogo.png"),
          ),
          Container(
            alignment: Alignment.bottomCenter,
            margin: EdgeInsets.only(bottom: 40),
            child: Text(
              "© Developed by Havatech",
              style: TextStyle(color: Colors.black, fontSize: 15),
            ),
          ),
          Container(
            height: double.infinity,
            width: double.infinity,
            color: Colors.black.withOpacity(fadeInAnimation.value),
          ),
        ],
      ),
    );
  }
}
