import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'SelectController.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SelectView extends StatefulWidget {
  final SelectController selectController;
  SelectView(this.selectController);
  @override
  _SelectViewState createState() => _SelectViewState();
}

class _SelectViewState extends State<SelectView> {
  checkPermission(BuildContext context , String title) async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.camera,
      Permission.microphone
    ].request();
    if (await Permission.camera.isGranted) {
      Navigator.pushNamed(context, '/barcodeReaderScreen' , arguments: {"title": title});
    }
  }

  @override
  Widget build(BuildContext context) {
    showAlert(BuildContext context){
      return Alert(
        context: context,
        type: AlertType.none,
        title: "Are you sure?",
        buttons: [
          DialogButton(
            child: Text(
              "NO",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () => Navigator.pop(context),
            color: Theme.of(context).primaryColor,
          ),
          DialogButton(
            child: Text(
              "YES",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () async {
              SharedPreferences prefs = await SharedPreferences.getInstance();
              prefs.setString('Token', '');
              prefs.setString('Username', '');
              prefs.setString('Password', '');
              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/loginScreen', (Route<dynamic> route) => false);
//                          Navigator.pop(context);
            },
            color: Theme.of(context).primaryColor,
          )
        ],
      ).show();
    }
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).canvasColor,
        elevation: 0,
        iconTheme: IconThemeData(color: Theme.of(context).primaryColor),
      ),
      drawer: Drawer(
        child: Container(
          color: Color.fromRGBO(242, 242, 242, 1),
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(),
              ListTile(
                leading: Icon(Icons.exit_to_app),
                title: Text('Log Out'),
                onTap: () {showAlert(context);},
              ),
            ],
          ),
        ),
      ),
      body:Center(
      child:Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _buildAddButton(context,),
          _buildExitButton(context,),
        ],
      ),
      ),
    );
  }

  Widget _buildAddButton(BuildContext context){
    return Container(
      margin: EdgeInsets.all(25),
      child:Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          RaisedButton(
            onPressed: (){checkPermission(context , 'add');},
            color: Theme.of(context).primaryColor,
            shape: CircleBorder(),
            padding: EdgeInsets.all(35),
            child: Image.asset(
              'assets/images/Baggage.png',
              color: Colors.white,
              height: 70,
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(height: 10,),
          Text('Add Baggage\n Bagaj Ekle', style: TextStyle(color: Colors.black, fontSize: 19,), textAlign: TextAlign.center,),
        ],),
    );
  }

  Widget _buildExitButton(BuildContext context){
    return Container(
      margin: EdgeInsets.all(25),
      child:Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          RaisedButton(
            onPressed: (){checkPermission(context , 'exit');},
            color: Theme.of(context).primaryColor,
            shape: CircleBorder(),
            padding: EdgeInsets.only(top: 35, right: 30, left: 40, bottom: 35),
            child: Image.asset(
              'assets/images/exit.png',
              color: Colors.white,
              height: 70,
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(height: 10,),
          Text('Exit Baggage\n Bagajdan çık', style: TextStyle(color: Colors.black, fontSize: 19,), textAlign: TextAlign.center,),
        ],),
    );
  }
}
