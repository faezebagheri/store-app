import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' show join;
import 'package:scoped_model/scoped_model.dart';
import 'package:path_provider/path_provider.dart';
import '../../global/MainModel.dart';
import 'AddBaggageController.dart';

class TakePictureScreen extends StatefulWidget {
  final CameraDescription camera;
  final AddBaggageController myController;

  TakePictureScreen(this.myController , this.camera);
  @override
  _TakePictureScreenState createState() => _TakePictureScreenState();
}

class _TakePictureScreenState extends State<TakePictureScreen> {
  CameraController _controller;
  Future<void> _initializeControllerFuture;
  File file;
  List<int> imageBytes;
  String base64Image;
  @override
  void initState() {
    super.initState();
    _controller = CameraController(
      widget.camera,
      ResolutionPreset.medium,
    );
    _initializeControllerFuture = _controller.initialize();
  }
  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(builder: (context, child, model)
    {
      void save(){
        model.setImage(base64Image);
      }
      return Scaffold(
        appBar: AppBar(
          title:Text('Photos'),
          centerTitle: true,
          iconTheme: IconThemeData(
              color: Theme.of(context).accentColor
          ),
        ),
        body: FutureBuilder<void>(
          future: _initializeControllerFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return CameraPreview(_controller);
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ),
        floatingActionButton: Align(
          alignment: Alignment.bottomCenter,
          child: FloatingActionButton(
            backgroundColor: Theme
                .of(context)
                .primaryColor,
            child: Icon(Icons.camera_alt, color: Theme
                .of(context)
                .accentColor,),
            onPressed: () async {
              try {
                await _initializeControllerFuture;
                final path = join(
                  (await getTemporaryDirectory()).path,
                  '${DateTime.now()}.png',
                );
                await _controller.takePicture(path);
                file = File(path);
                imageBytes = file.readAsBytesSync();
                base64Image = base64Encode(imageBytes);
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => DisplayPictureScreen(imagePath: path , save: save,),
                  ),
                );
              } catch (e) {
                print(e);
              }
            },
          ),
        ),
      );
    });
  }
}
class DisplayPictureScreen extends StatelessWidget {
  final String imagePath;
  final Function save;
  const DisplayPictureScreen({Key key, this.imagePath , this.save}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:Text('Photos'),
        centerTitle: true,
        iconTheme: IconThemeData(
            color: Theme.of(context).accentColor
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: FileImage(File(imagePath)),
            fit: BoxFit.cover
          )
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 25),
              child: RaisedButton(
                color: Colors.white,
                child: Text(
                  'Save',
                  style: TextStyle(
                    fontSize: 19,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                padding:
                EdgeInsets.symmetric(horizontal: 63.0, vertical: 10),
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(30), bottomLeft: Radius.circular(30)),
                ),
                onPressed: () {
                  save();
                  Navigator.of(context).pop();
//                  Navigator.pushReplacementNamed(context, '/addBaggageScreen');
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 25),
              child: RaisedButton(
                color: Colors.white,
                child: Text(
                  'Retake',
                  style: TextStyle(
                    fontSize: 19,
                      fontWeight: FontWeight.bold,
                  ),
                ),
                padding:
                EdgeInsets.symmetric(horizontal: 55.0, vertical: 10),
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(topRight: Radius.circular(30), bottomRight: Radius.circular(30)),
                ),
                onPressed: () {Navigator.pushReplacementNamed(context, '/takePictureScreen');},
              ),
            ),
            ],)
          ],
        ),
      ),

    );
  }
}
