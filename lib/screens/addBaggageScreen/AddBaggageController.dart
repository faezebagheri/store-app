import 'dart:convert';

import 'package:flutter/material.dart';

import '../../global/MainModel.dart';
import '../../global/MyController.dart';
import '../../utility/SocketController.dart';
import '../../utility/StandardString.dart';
import '../../utility/Consts.dart';

class AddBaggageController extends MyController {
  MainModel model;

  AddBaggageController(this.model);

  Image getImage(){
    return Image.memory(base64Decode(model.image));
  }

  Future<Null> selectedDate(BuildContext context) async {
    var date= new DateTime.now();
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: model.flightDate,
        firstDate: date.subtract(new Duration(days: 50)),
        lastDate: date.add(new Duration(days: 50)),
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.light().copyWith( //OK/Cancel button text color
                primaryColor: Theme.of(context).primaryColor, //Head background
                accentColor: Theme.of(context).primaryColor //selection color
            ),
            child: child,
          );
        }
    );
    if (picked != null ) {
      model.setFlightDate(picked);
    }
  }

  upload(BuildContext context) async{
    String reqData = "fdcsULU;" +
         model.barcode +
         ";" +
         model.image +
         ";" +
         model.flightNumber+
         ";" +
        standardDate(model.flightDate)+
        ";" +
        model.sequenceNumber +
        ";" +
        model.description +
        "//" +
        (model.airline == "" ? "NO" : model.airline) +
        ";" +
        model.token +
        ";" +
        model.firstName +" "+ model.lastName+
        ";" +
        "" +
        ";" +
        "" +
        ";" +
        model.route +
        ";" +
        "endU";
  print(reqData);
    var socketController = SocketController(
      context: context,
      onStart: onStartDefault,
      onEnd: onEndDefault,
      onSuccess: (responseData) => onSuccessConnect(responseData),
      onFailed: onFailedDefault,
      sendDataString: reqData,
      port: port,
      tcpAddress: tcpAddress,
      timeOutSeconds: 20,
      showBox: ()=> model.setShowBox(true)
    );

    await socketController.connectSocket();
  }

  onSuccessConnect(String responseData){
    print('Connect Success');
    model.setSuccessUpload(true);
  }

}