import 'package:baggagestore/utility/StandardString.dart';
import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:lottie/lottie.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import '../../global/MainModel.dart';
import 'AddBaggageController.dart';
import 'package:flutter/services.dart';
import '../../utility/Consts.dart';

class AddBaggageView extends StatefulWidget {
  final AddBaggageController myController;
  final CameraDescription camera;

  AddBaggageView(this.myController, this.camera);

  @override
  _AddBaggageViewState createState() => _AddBaggageViewState();
}

class _AddBaggageViewState extends State<AddBaggageView> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  bool showBox = false;
  bool showUploading = false;
  bool loading = false;
  final flightNumberFocus = FocusNode();
  final airlineFocus = FocusNode();
  final sequenceNumberFocus = FocusNode();
  final routeFocus = FocusNode();
  final firstNameFocus = FocusNode();
  final lastNameFocus = FocusNode();
  final descriptionFocus = FocusNode();
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(builder: (context, child, model) {
      return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          iconTheme: IconThemeData(color: Theme.of(context).accentColor),
          title: Text('Baggage information'),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.camera_alt,
                color: Theme.of(context).accentColor,
              ),
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  '/takePictureScreen',
                );
              },
            ),
          ],
        ),
        body: Stack(
          children: <Widget>[
            SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(16.0),
                child: Center(
                  child: _buildForm(model),
                ),
              ),
            ),
            model.showBox
                ?Stack(
              children: <Widget>[
                new Opacity(
                  opacity: 0.5 ,
                  child: new Container(
                    decoration: new BoxDecoration(
                        border: new Border.all(color: Colors.transparent),
                        color: new Color.fromRGBO(0, 0, 0, 0.7)
                    ),
                  ),
                ),
                _buildBottomBox(context, model),
              ],
            )
                :Center(),
          ],
        ),
      );
    });
  }

  Widget _buildBottomBox(BuildContext context, MainModel model) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Container(
          child: Material(
            color: Colors.white,
            elevation: 14.0,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(30),
              topLeft: Radius.circular(30),
            ),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                padding: EdgeInsets.only(
                  top: 20,
                  left: 5,
                  right: 5,
                  bottom: 10,
                ),
                child: model.successUpload
                    ? Container(
                  height: 300,
                  margin: EdgeInsets.only(left: 25, right: 25),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      GestureDetector(
//                              onTap: ()=> Navigator.of(context).pop(),
                        onTap: ()=>  Navigator.of(context).pushNamedAndRemoveUntil(
                            '/selectScreen', (Route<dynamic> route) => false),
                        child: Container(
                          height: 220,
                          child: Center(
                            child: FlareActor(
                              'assets/success.flr',
                              animation: 'success',
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 30,),
                      Center(child: Text('Upload Successfull', style: TextStyle(fontSize: 19, color: Colors.grey),)),
                      SizedBox(height: 25,)
                    ],
                  ),
                )
                    : Container(
                  height: 120,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Center(child: Text('Upload Failed!', style: TextStyle(fontSize: 19, color: Colors.grey),)),
                      SizedBox(height: 20,),
                      model.loading
                          ? Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: CircularProgressIndicator(backgroundColor: Theme.of(context).primaryColor,),
                      ):RaisedButton(
                        color: Theme.of(context).primaryColor,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Icon(Icons.refresh, color: Colors.white,),
                            SizedBox(width: 5,),
                            Text(
                              'Retry',
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white
                              ),
                            ),
                          ],
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(29),
                        ),
                        onPressed: () {
                          widget.myController.upload(context) ;
                          setState(() {
                            loading= true;
                          });
                        },
                        padding: EdgeInsets.symmetric(
                            horizontal: 30.0, vertical: 10),
                        materialTapTargetSize:
                        MaterialTapTargetSize.shrinkWrap,
                      ),
                      SizedBox(height: 10,),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildForm(MainModel model) {
    final flightNumberController =
    TextEditingController(text: model.flightNumber.toString());
    final airlineController =
    TextEditingController(text: model.airline.toString());
    final firstNameController =
    TextEditingController(text: model.firstName.toString());
    final lastNameController =
    TextEditingController(text: model.lastName.toString());
    final sequenceNumberController =
    TextEditingController(text: model.sequenceNumber.toString());
    final routeController = TextEditingController(text: model.route.toString());
    final descriptionController =
    TextEditingController(text: model.description.toString());
    final dateController = new TextEditingController(text: formatDate(model.flightDate));
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.only(top: 5),
        child: Column(
          children: <Widget>[
            _buildTextField(model , flightNumberController , airlineController , firstNameController,lastNameController ,
                routeController , descriptionController , sequenceNumberController, dateController),
            _buildDisplayPicture(model),
            model.loading
                ? Container(
              height: 45,
              child: Center(
                child: Lottie.asset('assets/uploading.json'),
              ),
            )
                : _buildUploadButton(context, model),
          ],
        ),
      ),
    );
  }

  Widget _buildTextField(MainModel model,flightNumberController , airlineController , firstNameController,lastNameController ,
      routeController , descriptionController , sequenceNumberController ,dateController) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 20,),
          child: Text("Flight Number / Uçuş numarası",style: TextStyle(fontSize: 15,color: Theme.of(context).primaryColor,),),
        ),
        TextFormField(
          style: TextStyle(fontSize: 19,color:Colors.black),
          focusNode: flightNumberFocus,
          textInputAction: TextInputAction.next,
          onFieldSubmitted: (value){
            FocusScope.of(context).requestFocus(airlineFocus);
          },
          keyboardType:TextInputType.number,
          inputFormatters: [new LengthLimitingTextInputFormatter(5),],
          controller: flightNumberController,
          decoration: InputDecoration(
            filled: true,
            fillColor: textBoxColor,
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),),
            focusColor: Theme.of(context).accentColor,
          ),
          onChanged: (value) {
            model.setFightNumber(value);
          },
          validator: (value) {
            if (value.isEmpty) {
              return 'Please Enter Flight Number';
            }
            return null;
          },
        ),
        SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20,),
          child: Text("Airline / Havayolu",style: TextStyle(fontSize: 15,color: Theme.of(context).primaryColor,),),
        ),
        TextFormField(
          style: TextStyle(fontSize: 19,color: Colors.black),
          focusNode: airlineFocus,
          textInputAction: TextInputAction.next,
          onFieldSubmitted: (value){
            FocusScope.of(context).requestFocus(firstNameFocus);
          },
          keyboardType:TextInputType.text,
          inputFormatters: [new LengthLimitingTextInputFormatter(2),],
          controller: airlineController,
          decoration: InputDecoration(
            filled: true,
            fillColor: textBoxColor,
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),),
            focusColor: Theme.of(context).accentColor,
          ),
          onChanged: (value) {
            model.setAirline(value);
          },
          validator: (value) {
            if (value.isEmpty) {
              return 'Please Enter Airline';
            }
            return null;
          },
        ),
        SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20,),
          child: Text("First Name / İsim",style: TextStyle(fontSize: 15,color: Theme.of(context).primaryColor,),),
        ),
        TextFormField(
          style: TextStyle(fontSize: 19,color: Colors.black,),
          focusNode: firstNameFocus,
          textInputAction: TextInputAction.next,
          onFieldSubmitted: (value){
            FocusScope.of(context).requestFocus(lastNameFocus);
          },
          keyboardType:TextInputType.text,
          controller: firstNameController,
          decoration: InputDecoration(
            filled: true,
            fillColor: textBoxColor,
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),),
            focusColor: Theme.of(context).accentColor,
          ),
          onChanged: (value) {
            model.setFirstName(value);
          },
        ),
        SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20,),
          child: Text("Last Name / Soyadı",style: TextStyle(fontSize: 15,color: Theme.of(context).primaryColor,),),
        ),
        TextFormField(
          style: TextStyle(fontSize: 19,color: Colors.black),
          focusNode: lastNameFocus,
          textInputAction: TextInputAction.next,
          onFieldSubmitted: (value){
            FocusScope.of(context).requestFocus(sequenceNumberFocus);
          },
          keyboardType:TextInputType.text,
          controller: lastNameController,
          decoration: InputDecoration(
            filled: true,
            fillColor: textBoxColor,
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),),
            focusColor: Theme.of(context).accentColor,
          ),
          onChanged: (value) {
            model.setLastName(value);
          },
        ),
        SizedBox(height: 20,),
        Padding(
          padding: const EdgeInsets.only(left: 20,),
          child: Text("Sequence Number / Sıra numarası",style: TextStyle(fontSize: 15,color: Theme.of(context).primaryColor,),),
        ),
        TextFormField(
          style: TextStyle(fontSize: 19,color: Colors.black),
          focusNode: sequenceNumberFocus,
          textInputAction: TextInputAction.next,
          onFieldSubmitted: (value){
            FocusScope.of(context).requestFocus(routeFocus);
          },
          keyboardType:TextInputType.number,
          inputFormatters: [new LengthLimitingTextInputFormatter(4),],
          controller: sequenceNumberController,
          decoration: InputDecoration(
            filled: true,
            fillColor: textBoxColor,
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),),
            focusColor: Theme.of(context).accentColor,
          ),
          onChanged: (value) {
            model.setSequenceNumber(value);
          },
        ),
        SizedBox(
          height: 20,
        ),
        _buildDateTextField(model, context, dateController),
        Padding(
          padding: const EdgeInsets.only(left: 20,),
          child: Text("Route / Rota",style: TextStyle(fontSize: 15,color: Theme.of(context).primaryColor,),),
        ),
        TextFormField(
          style: TextStyle(fontSize: 19,color:Colors.black),
          focusNode: routeFocus,
          textInputAction: TextInputAction.next,
          onFieldSubmitted: (value){
            FocusScope.of(context).requestFocus(descriptionFocus);
          },
          keyboardType:TextInputType.text,
          inputFormatters: [new LengthLimitingTextInputFormatter(7),],
          controller: routeController,
          decoration: InputDecoration(
            filled: true,
            fillColor: textBoxColor,
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),),
            focusColor: Theme.of(context).accentColor,
          ),
          onChanged: (value) {
            model.setRoute(value);
          },
        ),
        SizedBox(
          height: 40,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 50,
              width: 70,
              child: FittedBox(child: Image.asset(
                'assets/images/FlightNumber.png', fit: BoxFit.cover, color: Theme.of(context).primaryColor,)),
            ),
            SizedBox(width: 5,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Bagtag Data / Bagtag Verileri',
                  style: TextStyle(fontSize: 14, color: Theme.of(context).primaryColor,),),
                Text(model.barcode,
                  style: TextStyle(fontSize: 22, color: Theme.of(context).primaryColor,),)
              ],
            ),
          ],
        ),
        Divider(color: Theme.of(context).primaryColor,),
        SizedBox(height: 20,),
        TextFormField(
          style: TextStyle(
            fontSize: 19,
            color: Colors.black,
          ),
          controller: descriptionController,
          focusNode: descriptionFocus,
          maxLines: 4,
          keyboardType: TextInputType.multiline,
          decoration: InputDecoration(
            hintText: 'Description / Açıklama',
            hintStyle: TextStyle(color: Theme.of(context).primaryColor,),
            filled: true,
            fillColor: textBoxColor,
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            errorStyle:
            TextStyle(color: Theme.of(context).accentColor, fontSize: 15),
          ),
          onChanged: (value) {
            model.setDescription(value);
          },
          onSaved: (value) {
            model.setDescription(value);
          },
        ),
        SizedBox(
          height: 20,
        )
      ],
    );
  }

  Widget _buildDateTextField(MainModel model, BuildContext context,
      TextEditingController dateController) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(
            left: 20,
          ),
          child: Text(
            'Flight Date / Uçuş tarihi',
            style: TextStyle(
              fontSize: 15,
              color: Theme.of(context).primaryColor,
            ),
          ),
        ),
        GestureDetector(
          onTap: () async {
            await widget.myController.selectedDate(context);
            setState(() {});
          },
          child: AbsorbPointer(
            child: TextFormField(
              style: TextStyle(
                fontSize: 19,
                color: Colors.black,
              ),
              controller: dateController,
              keyboardType: TextInputType.datetime,
              decoration: InputDecoration(
                filled: true,
                fillColor: textBoxColor,
                contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(32.0),
                ),
                focusColor: Theme.of(context).primaryColor,
              ),
            ),
          ),
        ),
        SizedBox(
          height: 20,
        ),
      ],
    );
  }

  Widget _buildDisplayPicture(MainModel model) {
    return model.image == null
        ? Container()
        : Column(
      children: <Widget>[
        Align(
          alignment: Alignment.bottomLeft,
          child:model.image != '' ? Container(
              width: 120,
              height: 120,
              margin: EdgeInsets.only(top: 8, right: 10),
              decoration: BoxDecoration(
                border: Border.all(width: 1, color: Colors.grey),
              ),
              child: FittedBox(
                child: widget.myController.getImage(),
                fit: BoxFit.cover,
              )
          ): Center(),
        ),
        SizedBox(
          height: 20,
        ),
      ],
    );
  }

  Widget _buildUploadButton(BuildContext context, MainModel model,) {
    return RaisedButton(
      color: Theme.of(context).primaryColor,
      child:Text(
        'UPLOAD',
        style: TextStyle(
            fontSize: 17,
            fontWeight: FontWeight.bold,
            color: Colors.white
        ),
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(24),
      ),
      onPressed: () async {
        flightNumberFocus.unfocus();
        airlineFocus.unfocus();
        firstNameFocus.unfocus();
        lastNameFocus.unfocus();
        sequenceNumberFocus.unfocus();
        routeFocus.unfocus();
        descriptionFocus.unfocus();
        final isValid = _formKey.currentState.validate();
        if (!isValid) {
          if(model.flightNumber == ''){
            FocusScope.of(context).requestFocus(flightNumberFocus);
          }else if(model.airline.length < 2){
            FocusScope.of(context).requestFocus(airlineFocus);
          }
          return;
        }
        _formKey.currentState.save();
        if (model.image == null) {
          FocusScope.of(context).requestFocus(new FocusNode());
          Alert(
            context: context,
            type: AlertType.none,
            title: "Please Take a Photo / Lütfen Fotoğraf Çekin",
            buttons: [
              DialogButton(
                child: Text(
                  "Take photo",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                onPressed: () { Navigator.pop(context); Navigator.pushNamed(context, '/takePictureScreen');},
                color: Theme.of(context).primaryColor,
              ),
              DialogButton(
                child: Text(
                  "Send data",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                onPressed: () async {
                  model.setImage('');
                  Navigator.pop(context); setState(() {
                    showUploading = true;
                  });
                  await widget.myController.upload(context);
                  setState(() {
                    showBox = true;
                  });},
                color: Theme.of(context).primaryColor,
              )
            ],
          ).show();
        } else {
          setState(() {
            showUploading = true;
          });
          await widget.myController.upload(context);
          setState(() {
            showBox = true;
          });
        }
      },
      padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 4),
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
    );
  }
}
