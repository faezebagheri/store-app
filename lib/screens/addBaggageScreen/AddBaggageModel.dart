import 'package:scoped_model/scoped_model.dart';
import '../../utility/StandardString.dart';

mixin AddBaggageModel on Model {

  String _flightNumber='';
  String get flightNumber => _flightNumber;
  void setFightNumber(String number){
    _flightNumber = number;
  }

  String _airline='';
  String get airline => _airline;
  void setAirline(String airline){
    _airline = airline;
  }

  String _sequenceNumber='';
  String get sequenceNumber => _sequenceNumber;
  void setSequenceNumber(String sequence){
    _sequenceNumber = sequence;
  }

  String _firstName='';
  String get firstName => _firstName;
  void setFirstName(String firstName){
    _firstName = firstName;
  }

  String _lastName='';
  String get lastName => _lastName;
  void setLastName(String lastName){
    _lastName = lastName;
  }

  String _route='';
  String get route => _route;
  void setRoute(String route){
    _route = route;
  }

  String _description='';
  String get description => _description;
  void setDescription(String description){
    _description = description;
  }

  String _image='';
  String get image => _image;
  void setImage(String image){
    _image= image;
  }

  DateTime _flightDate = new DateTime.now();
  DateTime get flightDate => _flightDate;
  void setFlightDate(DateTime flightDate){
    _flightDate = flightDate;
  }

  String _flightDateString = '';
  String get flightDateString => _flightDateString;
  void setFlightDateString(String flightDateString){
    _flightDateString = flightDateString;
  }

  String _fullName='';
  String get fullName => _fullName;
  void setFullName(String fullName){
    _fullName = fullName;
  }
  String _base64ImageString='';
  String get base64ImageString =>_base64ImageString;
  void setBase64ImageString(String base64ImageString){
    _base64ImageString = base64ImageString;
  }

  String _baggageID='';
  String get baggageID => _baggageID;
  void setBaggageID(String baggageID){
    _baggageID = baggageID;
  }
}