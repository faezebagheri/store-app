import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../global/MainModel.dart';
import '../../global/MyController.dart';

class BarcodeReaderController extends MyController {
  MainModel model;

  BarcodeReaderController(this.model );
  String data ='';

  addBaggage(BuildContext context , code){
    model.setBarcode(code);
    Navigator.pushNamed(context,'/addBaggageScreen');
  }

  resetModel(){
    model.setFightNumber('');
    model.setAirline('');
    model.setFirstName('');
    model.setLastName('');
    model.setRoute('');
    model.setDescription('');
    model.setSequenceNumber('');
    model.setBaggageID('');
    model.setImage(null);
    model.setBarcode('');
    model.flightNumberList=[];
    model.flightDateList=[];
    model.airlineList=[];
    model.baggageIdList=[];
    model.exitDateTimeList=[];
    model.setFlightDate(DateTime.now());
    model.setFlightDateString('');
    model.setSuccessUpload(false);
    model.setShowBox(false);
    model.setHasList(false);
    model.setShowList(false);
    model.setBase64ImageString('');
  }
}