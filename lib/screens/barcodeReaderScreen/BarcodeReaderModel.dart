import 'package:scoped_model/scoped_model.dart';

mixin BarcodeReaderModel on Model {

  String _barcode="";
  String get barcode => _barcode;
  void setBarcode(String barcode) {
    _barcode = barcode;
  }
}