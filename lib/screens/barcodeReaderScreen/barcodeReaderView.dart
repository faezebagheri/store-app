import 'package:flutter/services.dart';

import '../../global/MainModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:barcode_reader/BarcodeScannerView.dart';
import 'BarcodeReaderController.dart';

class BarcodeReaderView extends StatefulWidget {
  final BarcodeReaderController myBarcodeReaderController;

  BarcodeReaderView(this.myBarcodeReaderController);

  @override
  State<StatefulWidget> createState() {
    return _BarcodeReaderViewState();
  }
}

class _BarcodeReaderViewState extends State<BarcodeReaderView> {
  bool showScanner = true;
  final GlobalKey<FormState> _formKey = GlobalKey();
  @override
  void initState(){
    super.initState();
    widget.myBarcodeReaderController.resetModel();
  }
  retrieveData(){
    String barcode = widget.myBarcodeReaderController.model.barcode;
    widget.myBarcodeReaderController.resetModel();
    widget.myBarcodeReaderController.model.setBarcode(barcode);
  }
  @override
  Widget build(BuildContext context) {
    precacheImage(AssetImage("assets/images/barcode.png"), context);
    final routArg = ModalRoute.of(context).settings.arguments as Map<String, String>;
    return ScopedModelDescendant<MainModel>(builder: (context, child, model) {
      final barcodeController =TextEditingController(text: model.barcode);
      return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          iconTheme: IconThemeData(color: Theme.of(context).accentColor),
          title: Text(
            'Barcode reader',
            style: TextStyle(color: Colors.white),
          ),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.format_size, color: Theme.of(context).accentColor, size: 35,),
                onPressed: () {
                  setState(() {
                    showScanner = false;
                  });
                }),
          ],
        ),
        body: (showScanner
            ? _buildScanner(model, context, routArg['title'])
            : _buildManualBarcode(model, routArg['title'],barcodeController)),
      );
    });
  }

  Widget _buildScanner(MainModel model, BuildContext context, String title) {
    String barcodeString;
    return !model.loading
      ?Container(
          child: BarcodeScannerView(
            onBarcodeRead:(code) {
              if(!model.loading) {
                barcodeString = code;
                String barcode = barcodeString.substring(
                    barcodeString.length - 6);
                model.setBarcode(barcode);
                if (title == 'exit') {
                  print('bacodeee: ' + model.barcode);
                  widget.myBarcodeReaderController.selectBaggage(context, "selectWithBarcode" , model.barcode);
                }
                if (title == 'add') {
                  print(model.barcode);
                  Navigator.pushNamed(context, '/addBaggageScreen').whenComplete(retrieveData());
                }
              }else{return;}
            }
          ),
        )
         :Center(
           child:Padding(
             padding: const EdgeInsets.all(5.0),
             child: CircularProgressIndicator(),
           )
         );
  }

  Widget _buildManualBarcode(MainModel model, String title,barcodeController) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Material(
            elevation: 14.0,
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(25),
              topLeft: Radius.circular(25),
            ),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                padding: EdgeInsets.only(top: 28,left: 5,right: 5,bottom: 15,),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Image.asset(
                        'assets/images/barcode.png',
                        color: Colors.white,
                        width: 350,
                        height: 55,
                        fit: BoxFit.cover,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 20, right: 20),
                        child: _buildForm(model, title,barcodeController),
                      ),
                    ]),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildForm(MainModel model, String title,barcodeController) {
    return Form(
      key: _formKey,
      child: Column(children: <Widget>[
        Container(
          margin: EdgeInsets.only(left: 12,
          right: 12),
          child: TextFormField(
            style: TextStyle(
              fontSize: 19,
              color: Colors.black,
            ),
            inputFormatters: [
              new LengthLimitingTextInputFormatter(6),
            ],
            keyboardType: TextInputType.number,
            controller: barcodeController,
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              errorStyle:
              TextStyle(color: Theme.of(context).accentColor, fontSize: 14),
            ),
            onSaved: (value) => model.setBarcode(value),
            onChanged: (value) => model.setBarcode(value),
            validator: (value) {
              if (value.length <= 5) {
                return 'The barcode lenght must be at least 6';
              }
            },
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 15, right: 13, left: 13),
              child: RaisedButton(
                onPressed: () {
                  setState(() {
                    showScanner = true;
                  });
                },
                color: Theme.of(context).canvasColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Text(
                  'RESCAN',
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                  ),
                ),
                padding: EdgeInsets.only(top: 12,bottom: 12, right: 15 ,left: 15),
              ),
            ),
            model.loading
            ? Padding(
              padding: const EdgeInsets.all(5.0),
              child: CircularProgressIndicator(),
            )
            : Container(
              margin: EdgeInsets.only(top: 15, right: 13, left: 13),
              child: RaisedButton(
                onPressed: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                  final isValid = _formKey.currentState.validate();
                  if (!isValid) {
                    return;
                  }
                  _formKey.currentState.save();
                  if (title == 'exit') {
                    widget.myBarcodeReaderController.selectBaggage(context , "selectWithBarcode" , model.barcode);
                  }
                  if (title == 'add') {
                    print(model.barcode);
                    Navigator.pushNamed(context, '/addBaggageScreen').whenComplete(retrieveData());
                  }
                },
                color: Theme.of(context).canvasColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Text(
                  'CONFIRM',
                  style: TextStyle(
                    fontSize: 16,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
                padding: EdgeInsets.only(top: 12,bottom: 12, right: 15 ,left: 15),
              ),
            ),
          ],
        ),
      ]),
    );
  }
}

