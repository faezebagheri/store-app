import 'package:scoped_model/scoped_model.dart';

mixin ExitBaggageModel on Model{

  String _exitDateTime = '';
  String get exitDateTime => _exitDateTime;
  void setExitDateTime(String exitDateTime){
    _exitDateTime = exitDateTime;
  }

  List flightNumberList = [];
  List flightDateList = [];
  List baggageIdList = [];
  List airlineList = [];
  List exitDateTimeList =[];

  bool _hasList = false;
  bool get hasList => _hasList;
  void setHasList(bool hasList){
    _hasList = hasList;
  }
}