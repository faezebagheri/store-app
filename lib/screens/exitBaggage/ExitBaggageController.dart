import 'package:baggagestore/utility/ExitBaggage.dart';
import 'package:flutter/material.dart';

import '../../global/MainModel.dart';
import '../../global/MyController.dart';
import '../../utility/ExitBaggage.dart';
import '../../utility/Consts.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class ExitBaggageController extends MyController{
  MainModel model;
  ExitBaggageController(this.model);
  String data ='';

  exitBaggage(BuildContext context) async{
    final Map<String, dynamic> reqData = {
      "Token": model.token,
      "BaggageID":model.baggageID,
    };

    var exit = ExitBaggage(
        api: rootAPI + exitAPI,
        onEnd: onEndDefault,
        onFailed: onFailedDefault,
        onStart: onStartDefault,
        onSuccess: (responseData) => onSuccessExit(context, responseData),
        req: reqData,
        context: context);


    exit.request();
  }
  onSuccessExit(BuildContext context, dynamic resultData) {
    print("Success");
      Alert(
        context: context,
        type: AlertType.none,
        title: "Done",
        buttons: [
          DialogButton(
            child: Text(
              "OK",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            color: Theme.of(context).primaryColor,
            onPressed: () {
              Navigator.pop(context);
              if(model.showList){
                model.flightNumberList=[];
                model.flightDateList=[];
                model.airlineList=[];
                model.baggageIdList=[];
                model.exitDateTimeList=[];
                selectBaggage(context, "selectWithBarcode", model.barcode);
                Navigator.of(context).pop();
              }else {
                Navigator.of(context).pop();
              }
            },
            width: 120,
          )
        ],
      ).show();
  }
}