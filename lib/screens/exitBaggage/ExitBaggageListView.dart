import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'ExitBaggageController.dart';
import '../../global/MainModel.dart';

class ExitBaggageListView extends StatefulWidget {
  final ExitBaggageController myController;
  ExitBaggageListView(this.myController);
  @override
  _ExitBaggageListViewState createState() => _ExitBaggageListViewState();
}

class _ExitBaggageListViewState extends State<ExitBaggageListView> {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(builder: (context, child, model){
      return Scaffold(
        appBar: AppBar(
          title: Text('Baggage information'),
          centerTitle: true,
          iconTheme: IconThemeData(color: Theme.of(context).accentColor),
        ),
        body: Stack(
          children: <Widget>[
            Container(
              child: Container(
                padding: EdgeInsets.only(top: 20,left: 10,right: 10,bottom: 10,),
                height: MediaQuery.of(context).size.height,
                color: Color.fromRGBO(242, 242, 242, 1),
                child:ListView.builder(
                    itemCount:  model.airlineList.length,
                    itemBuilder:(context , index){
                      return GestureDetector(
                        onTap: (){widget.myController.selectBaggage(context, "selectWithBaggageID" ,model.baggageIdList[index]);},
                        child: Card(
                            elevation: 5,
                            margin: EdgeInsets.symmetric(
                              vertical: 8,
                              horizontal: 5,
                            ),
                            child: Column(
                              children: <Widget>[
                                SizedBox(height: 10,),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                        height: 48,
                                        width: 70,
                                        child: Image.network("http://appdcsapi.abomis.com/api/GetAirlineImage/5?nullForNotExists=true&airlineCode=" + model.airlineList[index])
                                    ),
                                    SizedBox(width: 5,),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text('Airline / Havayolu',
                                          style: TextStyle(fontSize: 11, color: Colors.black),),
                                        Text(model.airlineList[index],
                                          style: TextStyle(fontSize: 22, color: Colors.black),)
                                      ],
                                    ),
                                  ],
                                ),
                                Divider(color: Colors.grey,),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      height: 48,
                                      width: 70,
                                      child: FittedBox(child: Image.asset(
                                        'assets/images/FlightNumber.png', fit: BoxFit.cover, color: Theme.of(context).primaryColor,)),
                                    ),
                                    SizedBox(width: 5,),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text('Flight Number / Uçuş numarası',
                                          style: TextStyle(fontSize: 11, color: Colors.black),),
                                        Text(model.flightNumberList[index],
                                          style: TextStyle(fontSize: 22, color: Colors.black),)
                                      ],
                                    ),
                                  ],
                                ),
                                Divider(color: Colors.grey,),
                                SizedBox(height: 5,),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                        height: 48,
                                        width: 70,
                                        child: FittedBox(child: Image.asset(
                                          'assets/images/FlightDate.png', fit: BoxFit.cover, color: Theme.of(context).primaryColor,))
                                    ),
                                    SizedBox(width: 5,),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text('Flight Date / Uçuş tarihi',
                                          style: TextStyle(fontSize: 11, color: Colors.black),),
                                        Container(width: 220,
                                            child: Text(model.flightDateList[index],
                                              style: TextStyle(fontSize: 21, color: Colors.black),
                                              overflow: TextOverflow.ellipsis,))
                                      ],
                                    ),
                                  ],
                                ),
                                Divider(color: Colors.grey,),
                                SizedBox(height: 10,),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.only(bottom: 8),
                                      height: 48,
                                      width: 70,
                                      child: FittedBox(child: Image.asset(
                                        'assets/images/exit-icon.png', fit: BoxFit.cover, color: Theme.of(context).primaryColor,)),
                                    ),
                                    SizedBox(width: 5,),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text((model.exitDateTimeList[index] == '') ?'' : 'Exit at date time / Çıkış saatleri', style: TextStyle(fontSize: 11, color: Colors.black),),
                                        Container( width: 230,
                                          child: Text((model.exitDateTimeList[index] == '') ?'Not exit / Çıkış yok' : model.exitDateTimeList[index],
                                            style: TextStyle(fontSize: 21, color: Colors.black),overflow: TextOverflow.ellipsis,),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            )
                        ),
                      );
                    }
                ),
              ),
            ),
            model.loading
                ? Stack(
              children: <Widget>[
                new Opacity(opacity: 0.3,
                  child: new Container(
                    decoration: new BoxDecoration(
                        border: new Border.all(color: Colors.transparent),
                        color: new Color.fromRGBO(0, 0, 0, 0.5)
                    ),
                  ),
                ),
                Center(
                  child: CircularProgressIndicator(),
                )
              ],
            )
                :Center()
          ],
        ),
      );
    });
  }
}
