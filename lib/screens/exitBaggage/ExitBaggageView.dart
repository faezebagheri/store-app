import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'ExitBaggageController.dart';
import '../../global/MainModel.dart';

class ExitBaggageView extends StatefulWidget {
  final ExitBaggageController myController;

  ExitBaggageView(this.myController);

  @override
  _ExitBaggageViewState createState() => _ExitBaggageViewState();
}

class _ExitBaggageViewState extends State<ExitBaggageView> {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(builder: (context, child, model) {
      final descriptionController =
      TextEditingController(text: model.description.toString());
      return Scaffold(
        appBar: AppBar(
          title: Text('Baggage information'),
          centerTitle: true,
          iconTheme: IconThemeData(color: Theme.of(context).accentColor),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: IconButton(
                icon: Icon(Icons.exit_to_app,
                  color: Theme.of(context).accentColor, size: 32,),
                onPressed: () => widget.myController.exitBaggage(context),
              ),
            ),
          ],
        ),
        body: Stack(
          children: <Widget>[
            SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(30.0),
                child: Center(
                  child: _buildView(model, context, descriptionController),
                ),
              ),
            ),
            model.loading
                ?Stack(
              children: <Widget>[
                new Opacity(
                  opacity: 0.5 ,
                  child: new Container(
                    decoration: new BoxDecoration(
                        border: new Border.all(color: Colors.transparent),
                        color: new Color.fromRGBO(0, 0, 0, 0.7)
                    ),
                  ),
                ),
                Center(
                  child: CircularProgressIndicator(),
                )
              ],
            )
                :Center(),
          ],
        ),
      );
    });
  }

  Widget _buildView(MainModel model, BuildContext context,
      descriptionController) {
    const String aLImageAPI = "http://appdcsapi.abomis.com/api/GetAirlineImage/5?nullForNotExists=true&airlineCode=";
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 48,
              width: 70,
              child: FittedBox(child: Image.network(aLImageAPI + model.airline, fit: BoxFit.cover,)),
            ),
            SizedBox(width: 5,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Flight Number / Uçuş numarası',
                  style: TextStyle(fontSize: 11, color: Colors.black),),
                Text(model.flightNumber,
                  style: TextStyle(fontSize: 23, color: Colors.black),)
              ],
            ),
          ],
        ),
        Divider(color: Colors.grey,),
        SizedBox(height: 10,),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                height: 48,
                width: 70,
                child: FittedBox(child: Image.asset(
                  'assets/images/FlightDate.png', fit: BoxFit.cover, color: Theme.of(context).primaryColor,))
            ),
            SizedBox(width: 5,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Flight Date / Uçuş tarihi',
                  style: TextStyle(fontSize: 11, color: Colors.black),),
                Container(width: 220,
                    child: Text(model.flightDateString,
                      style: TextStyle(fontSize: 23, color: Colors.black),
                      overflow: TextOverflow.ellipsis,))
              ],
            ),
          ],
        ),
        Divider(color: Colors.grey,),
        SizedBox(height: 10,),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                height: 48,
                width: 70,
                child: FittedBox(child: Image.asset(
                  'assets/images/SecurityNumber.png', fit: BoxFit.cover, color: Theme.of(context).primaryColor,))
            ),
            SizedBox(width: 5,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Sequence Number / Sıra numarası',
                  style: TextStyle(fontSize: 11, color: Colors.black),),
                Text(model.sequenceNumber,
                  style: TextStyle(fontSize: 23, color: Colors.black),)
              ],
            ),
          ],
        ),
        Divider(color: Colors.grey,),
        SizedBox(height: 10,),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 48,
              width: 70,
              child: FittedBox(child: Image.asset(
                'assets/images/FlightNumber.png', fit: BoxFit.cover, color: Theme.of(context).primaryColor,)),
            ),
            SizedBox(width: 5,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Bagtag Data / Bagtag Verileri',
                  style: TextStyle(fontSize: 11, color: Colors.black),),
                Text(model.barcode,
                  style: TextStyle(fontSize: 22, color: Colors.black),)
              ],
            ),
          ],
        ),
        Divider(color: Colors.grey,),
        SizedBox(height: 10,),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                height: 48,
                width: 70,
                child: Icon(Icons.person_outline, size: 46, color: Theme
                    .of(context)
                    .primaryColor,)
            ),
            SizedBox(width: 5,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Name / isim', style: TextStyle(fontSize: 11, color: Colors.black),),
                Text(model.fullName,
                  style: TextStyle(fontSize: 22, color: Colors.black),)
              ],
            ),
          ],
        ),
        Divider(color: Colors.grey,),
        SizedBox(height: 10,),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                height: 48,
                width: 70,
                child: Icon(Icons.airplanemode_active, size: 46, color: Theme
                    .of(context)
                    .primaryColor,)
            ),
            SizedBox(width: 5,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Airline / Havayolu',
                  style: TextStyle(fontSize: 11, color: Colors.black),),
                Text(model.airline,
                  style: TextStyle(fontSize: 22, color: Colors.black),)
              ],
            ),
          ],
        ),
        Divider(color: Colors.grey,),
        SizedBox(height: 10,),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                height: 48,
                width: 70,
                child: Icon(Icons.location_on, size: 46, color: Theme
                    .of(context)
                    .primaryColor,)
            ),
            SizedBox(width: 5,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Route / Rota',
                  style: TextStyle(fontSize: 11, color: Colors.black),),
                Text(model.route,
                  style: TextStyle(fontSize: 22, color: Colors.black),)
              ],
            ),
          ],
        ),
        Divider(color: Colors.grey,),
        SizedBox(height: 10,),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                padding: EdgeInsets.only(bottom: 8),
                height: 48,
                width: 70,
                child: FittedBox(child: Image.asset(
                  'assets/images/exit-icon.png', fit: BoxFit.cover, color: Theme.of(context).primaryColor,)),
            ),
            SizedBox(width: 5,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text((model.exitDateTime == '') ?'' : 'Exit at date time / Çıkış saatleri', style: TextStyle(fontSize: 11, color: Colors.black),),
                Container( width: 230,
                  child: Text((model.exitDateTime == '') ?'Not exit / Çıkış yok' : model.exitDateTime,
                    style: TextStyle(fontSize: 21, color: Colors.black),overflow: TextOverflow.ellipsis,),
                )
              ],
            ),
          ],
        ),
        Divider(color: Colors.white,),
        SizedBox(height: 5,),
        TextField(
          style: TextStyle(
            fontSize: 15,
            color: Colors.black,
          ),
          maxLines: 5,
          controller: descriptionController,
          enabled: false,
          decoration: InputDecoration(
            hintText: 'Description / Açıklama',
            filled: true,
            fillColor: Colors.white,
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
          ),
          onChanged: (value) {
            descriptionController.text = model.description;
          },
        ),
        model.image != ''
        ?Column(
          children: <Widget>[
            SizedBox(
              height: 10,
            ),
            Container(
              width: double.infinity,
              height: 350,
              margin: EdgeInsets.only(top: 8, right: 10),
              decoration: BoxDecoration(
                border: Border.all(width: 1, color: Colors.grey),
              ),
              child: FittedBox(
                child: widget.myController.getImage(),
                fit: BoxFit.cover,
              ),
            ),
          ],
        )
        :Center(),
      ],
    );
  }
}
