import 'dart:async';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import 'global/MainModel.dart';
import 'screens/loginScreen/LoginController.dart';
import 'screens/barcodeReaderScreen/BarcodeReaderController.dart';
import 'screens/loginScreen/LoginView.dart';
import 'screens/selectScreen/selectView.dart';
import 'screens/selectScreen/SelectController.dart';
import 'screens/addBaggageScreen/AddBaggageView.dart';
import 'screens/addBaggageScreen/AddBaggageController.dart';
import 'screens/barcodeReaderScreen/BarcodeReaderView.dart';
import 'screens/addBaggageScreen/TakePictureScreen.dart';
import 'screens/exitBaggage/ExitBaggageController.dart';
import 'screens/exitBaggage/ExitBaggageView.dart';
import 'screens/splashScreen/SplashController.dart';
import 'screens/splashScreen/SplashView.dart';
import 'screens/exitBaggage/ExitBaggageListView.dart';

Future<void> main() async  {
  WidgetsFlutterBinding.ensureInitialized();
  final cameras = await availableCameras();
  final firstCamera = cameras.first;
  runApp(MyApp(firstCamera));
}
MainModel _model = MainModel();
class MyApp extends StatelessWidget {

  final CameraDescription camera;
  MyApp(this.camera);
  @override
  Widget build(BuildContext context) {
    return ScopedModel<MainModel>(
      model: _model,
          child: MaterialApp(
        title: 'Luggage Storage',
        theme: ThemeData(
          // primarySwatch: Colors.white,
          primaryColor: Color.fromRGBO(30, 83, 167, 1),
          accentColor: Colors.white,
//          canvasColor:Color.fromRGBO(50, 50, 50, 1),
          errorColor: Color.fromRGBO(30, 83, 167, 1),
          fontFamily: 'Raleway',
          textTheme: TextTheme(
            display1: TextStyle(
              color: Colors.white,
              fontSize: 16,
            ),
          ),
        ),
        home: SplashView(SplashController(_model)),
//        home: ExitBaggageView(ExitBaggageController(_model)),
        routes: {
          '/loginScreen': (BuildContext context) =>
              LoginView(LoginController(_model)),
          '/selectScreen': (BuildContext context) =>
              SelectView(SelectController(_model)),
          '/barcodeReaderScreen': (BuildContext context) =>
              BarcodeReaderView(BarcodeReaderController(_model)),
          '/addBaggageScreen': (BuildContext context) =>
              AddBaggageView(AddBaggageController(_model ) ,camera),
          '/takePictureScreen': (BuildContext context) =>
              TakePictureScreen(AddBaggageController(_model ) , camera),
          '/exitBaggageScreen': (BuildContext context) =>
              ExitBaggageView(ExitBaggageController(_model)),
          '/ExitBaggageListView': (BuildContext context)=>
              ExitBaggageListView(ExitBaggageController(_model)),
        },
      ),
    );
  }
}
